// Copyright 2022 James Cote
// All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package auth

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/jfcote87/ctxclient"
	"github.com/jfcote87/oauth2"
)

const defaultTokenDuration = 4 * time.Hour

// Credentials implements a simple caching structure
// for oauth tokens.
type Credentials struct {
	tc            TokenCache
	ts            oauth2.TokenSource
	tokenDuration time.Duration
	memToken      *oauth2.Token
	cl            *http.Client
	m             sync.Mutex
}

// TokenCache provides the methods for saving and retreiving
// tokens
type TokenCache interface {
	Save(context.Context, *oauth2.Token) error
	Get(context.Context) (*oauth2.Token, error)
}

type nullCache struct{}

func (nc nullCache) Get(context.Context) (*oauth2.Token, error) {
	return nil, nil
}

func (nc nullCache) Save(context.Context, *oauth2.Token) error {
	return nil
}

// CachingClientFunc returns ctxclient func returning a token caching http.Client
func CachingClientFunc(tc TokenCache, ts oauth2.TokenSource, op ...ClientFuncParam) (ctxclient.Func, error) {
	if tc == TokenCache(nil) {
		tc = nullCache{}
	}
	cc := &Credentials{
		tc:            tc,
		ts:            ts,
		tokenDuration: defaultTokenDuration,
	}
	for _, o := range op {
		o(cc)
	}
	if cc.ts == nil {
		return nil, fmt.Errorf("tokensource may not be nil unless an initial token is provided")
	}
	return cc.Client, nil
}

// ClientFuncParam update a clientFunc
type ClientFuncParam func(*Credentials)

// TokenCacheDuration set the duration time for a new token
func TokenCacheDuration(dur time.Duration) ClientFuncParam {
	return func(cx *Credentials) {
		cx.tokenDuration = dur
	}
}

// InitialToken sets the initial token for a client caching object
func InitialToken(tk *oauth2.Token) ClientFuncParam {
	return func(cx *Credentials) {
		if cx.ts == nil {
			cx.ts = oauth2.StaticTokenSource(tk)
			return
		}
		cx.memToken = tk
	}
}

// Token returns a valid oauth token for authorizing a restapi call
func (cc *Credentials) Token(ctx context.Context) (*oauth2.Token, error) {
	cc.m.Lock()
	defer cc.m.Unlock()
	// Check memory cache
	if cc.memToken.Valid() {
		return cc.memToken, nil
	}
	// retrieve token from cache
	token, err := cc.tc.Get(ctx)
	if err != nil {
		return nil, err
	}
	// if invalid get new token
	if token.Valid() {
		cc.memToken = token
		return token, nil
	}

	if token, err = cc.ts.Token(ctx); err != nil {
		return nil, err
	}
	if token.Expiry.IsZero() && cc.tokenDuration > 0 {
		token.Expiry = time.Now().Add(cc.tokenDuration)
	}

	if err := cc.tc.Save(ctx, token); err != nil {
		return nil, err
	}

	cc.memToken = token
	return token, nil
}

// Client returns an http client that authorizes calls using the credentials
// reuse and caching logic.  May be used as a ctxclient.Func.
func (cc *Credentials) Client(ctx context.Context) (*http.Client, error) {
	cc.m.Lock()
	defer cc.m.Unlock()
	if cc.cl == nil {
		cc.cl = oauth2.Client(cc, nil)
	}
	return cc.cl, nil

}

// FileCache creates a file based cache
type FileCache struct {
	Filename string
	// Don't report save error so not stopping program
	IgnorePersistsErr bool
	m                 sync.Mutex
}

// Save overwrites the cache file
func (fc *FileCache) Save(ctx context.Context, tk *oauth2.Token) error {
	fc.m.Lock()
	defer fc.m.Unlock()
	f, err := os.Create(fc.Filename)
	if err == nil {
		defer f.Close()
		ex := json.NewEncoder(f)
		ex.SetIndent("", "    ")
		err = ex.Encode(tk)
	}
	if fc.IgnorePersistsErr {
		return nil
	}
	return err
}

// Get decodes file content into a token
func (fc *FileCache) Get(ctx context.Context) (*oauth2.Token, error) {
	fc.m.Lock()
	defer fc.m.Unlock()
	f, err := os.Open(fc.Filename)
	if err != nil {
		if strings.HasSuffix(err.Error(), "no such file or directory") {
			err = nil
		}
		return nil, err
	}
	defer f.Close()
	var tk *oauth2.Token
	return tk, json.NewDecoder(f).Decode(&tk)
}

// ServiceFromPassword creates a service that authenticates using a token created from
// username and password. More details may be found at:
// https://help.salesforce.com/s/articleView?id=sf.remoteaccess_oauth_username_password_flow.htm&type=5
