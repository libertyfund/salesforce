// Copyright 2022 James Cote
// All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package auth_test

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"sync"
	"testing"
	"time"

	"bitbucket.org/libertyfund/salesforce/auth"
	"github.com/jfcote87/ctxclient"
	"github.com/jfcote87/oauth2"
)

type testTS struct {
	expectedToken string
	hasErr        bool
	cnt           int
	curToken      *oauth2.Token
	m             sync.Mutex
}

func (t *testTS) Token(ctx context.Context) (*oauth2.Token, error) {
	if t.hasErr {
		t.hasErr = false
		return nil, errors.New("token error")
	}
	t.cnt++
	t.curToken = &oauth2.Token{AccessToken: fmt.Sprintf("TOKEN%03d", t.cnt)}
	return t.curToken, nil
}

type testCache struct {
	TK         *oauth2.Token
	hasGetErr  bool
	hasSaveErr bool
	m          sync.Mutex
}

func (tc *testCache) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	auth := r.Header.Get("Authorization")
	w.Header().Set("Content-type", "application/json")
	json.NewEncoder(w).Encode(&oauth2.Token{AccessToken: strings.Replace(auth, "Bearer ", "", 1)})
}

func (tc *testCache) Get(ctx context.Context) (*oauth2.Token, error) {
	tc.m.Lock()
	defer tc.m.Unlock()
	if tc.hasGetErr {
		tc.TK = nil
		tc.hasGetErr = false
		return nil, errors.New("get error")
	}
	if !tc.TK.Valid() {
		tc.TK = nil
	}
	return tc.TK, nil
}

func (tc *testCache) Save(ctx context.Context, tk *oauth2.Token) error {
	tc.m.Lock()
	defer tc.m.Unlock()
	if tc.hasSaveErr {
		tc.hasSaveErr = false
		return errors.New("save error")
	}
	tc.TK = tk
	return nil
}

func TestCachingClientFunc(t *testing.T) {
	tests := []struct {
		name    string
		tc      auth.TokenCache
		ts      oauth2.TokenSource
		op      []auth.ClientFuncParam
		wantErr bool
	}{
		{name: "test00", wantErr: true},
		{name: "test01", tc: &testCache{}, ts: nil, wantErr: true},
		{name: "test02", tc: &testCache{}, ts: nil, op: []auth.ClientFuncParam{auth.InitialToken(&oauth2.Token{AccessToken: "ABC"})}},
		{name: "test03", tc: &testCache{}, ts: &testTS{}, op: []auth.ClientFuncParam{auth.TokenCacheDuration(time.Hour)}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := auth.CachingClientFunc(tt.tc, tt.ts, tt.op...)
			if tt.wantErr {
				if err == nil {
					t.Errorf("%s: expected tokensource error; got nil error", tt.name)
				}
				return
			}
			if err != nil {
				t.Errorf("%s: expected success; got %v", tt.name, err)
			}
		})
	}
}

func TestCredentials(t *testing.T) {
	tsx := &testTS{}

	var tc = &testCache{}
	srv := httptest.NewServer(tc)
	var tcx auth.TokenCache = tc
	defer srv.Close()
	urlx := srv.URL
	ctx := context.Background()

	var cf ctxclient.Func
	ftkError := func() {
		tsx.hasErr = true
	}
	setCache := func() {
		tc.TK = &oauth2.Token{AccessToken: "XToken"}
	}
	expireCache := func() {
		tc.TK.Expiry = time.Now()
	}
	_ = ftkError
	var initToken2 = &oauth2.Token{AccessToken: "STATIC2"}

	var opsDefault = []auth.ClientFuncParam{auth.TokenCacheDuration(time.Hour)}
	var opsInit = []auth.ClientFuncParam{auth.InitialToken(&oauth2.Token{AccessToken: "STATIC"})}
	var opsInit2 = []auth.ClientFuncParam{auth.InitialToken(initToken2)}
	clearToken2 := func() {
		tc.TK = nil
		initToken2.Expiry = time.Now()
	}
	clearToken3 := func() {
		tc.hasGetErr = true
		tc.hasSaveErr = true
		tsx.curToken.Expiry = time.Now()
	}
	removeTokenCache := func() {
		tsx.curToken.Expiry = time.Now()
		tcx = nil
	}

	tests := []struct {
		name       string
		token      string
		haserr     bool
		recreateCF bool
		op         []auth.ClientFuncParam
		do         func()
	}{
		{name: "test00", token: "STATIC", recreateCF: true, op: opsInit},
		{name: "test01", haserr: true, do: ftkError, recreateCF: true, op: opsDefault},
		{name: "test02", token: "TOKEN001"},
		{name: "test03", token: "TOKEN001"},
		{name: "test04", token: "XToken", do: setCache, recreateCF: true, op: opsDefault},
		{name: "test05", token: "TOKEN002", do: expireCache},
		{name: "test06", token: "STATIC2", recreateCF: true, op: opsInit2},
		{name: "test07", token: "TOKEN003", do: clearToken2},
		{name: "test08", haserr: true, do: clearToken3},
		{name: "test09", haserr: true},
		{name: "test10", token: "TOKEN005", do: removeTokenCache, recreateCF: true, op: opsDefault},
	}
	for _, tt := range tests {
		if tt.do != nil {
			tt.do()
		}
		if tt.recreateCF {
			cx, err := auth.CachingClientFunc(tcx, tsx, tt.op...)
			if err != nil {
				t.Errorf("expected succesful reinitializing CachingClientFunc; got %v", err)
				continue
			}
			cf = cx
		}
		t.Run(tt.name, func(t *testing.T) {
			tk, err := getToken(ctx, cf, urlx)
			if err != nil {
				if !tt.haserr {
					t.Errorf("%s: expected token %s; got %v", tt.name, tt.token, err)
				}
				return
			}
			if tt.token != tk.AccessToken {
				t.Errorf("%s: expected token %s; got %v", tt.name, tt.token, tk.AccessToken)
			}
		})
	}
}

func getToken(ctx context.Context, cf ctxclient.Func, urlx string) (*oauth2.Token, error) {
	var token *oauth2.Token
	rx, _ := http.NewRequest("GET", urlx, nil)
	res, err := cf.Do(ctx, rx)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	return token, json.NewDecoder(res.Body).Decode(&token)
}

func TestFileCache(t *testing.T) {
	tmpDir, err := os.MkdirTemp("", "cache")
	if err != nil {
		t.Errorf("mkdirtemp failed: %v", err)
	}
	baseName := tmpDir + "/token.json"
	fc := &auth.FileCache{
		Filename: baseName,
	}
	fnError := func() {
		fc.Filename = baseName + "/a/a/a"
	}
	ignorePersistError := func() {
		fnError()
		fc.IgnorePersistsErr = true
	}

	tests := []struct {
		name    string
		token   string
		persist bool
		fn      func()
		wantErr bool
	}{
		{name: "test00", token: "nil"},
		{name: "test01", persist: true, token: "TOKEN_A"},
		{name: "test02", token: "TOKEN_A", wantErr: false},
		{name: "test03", persist: true, fn: fnError, wantErr: true},
		{name: "test04", persist: true, fn: ignorePersistError},
	}
	ctx := context.Background()
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.fn != nil {
				tt.fn()
			}
			if tt.persist {
				err := fc.Save(ctx, &oauth2.Token{AccessToken: tt.token})
				if tt.wantErr {
					if err == nil {
						t.Errorf("%s: expected error; got success", tt.name)
					}
					return
				}
				if err != nil {
					t.Errorf("%s: expected success; got %v", tt.name, err)
				}
				return
			}
			tk, err := fc.Get(ctx)
			if err != nil {
				if !tt.wantErr {
					t.Errorf("%s: expected success; got %v", tt.name, err)
				}
				return
			}
			if tt.wantErr {
				t.Errorf("%s: expected error; got success", tt.name)
				return
			}
			if getTokenString(tk) != tt.token {
				t.Errorf("%s: expected %s; got %s", tt.name, tt.token, tk.AccessToken)
			}
		})
	}
}

func getTokenString(tk *oauth2.Token) string {
	if tk == nil {
		return "nil"
	}
	return tk.AccessToken
}
