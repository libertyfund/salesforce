// Copyright 2022 James Cote
// All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package jwt

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"time"

	"bitbucket.org/libertyfund/salesforce"
	"bitbucket.org/libertyfund/salesforce/auth"
	"github.com/jfcote87/ctxclient"
	"github.com/jfcote87/oauth2/jws"
	"github.com/jfcote87/oauth2/jwt"
)

const hostTest = "https://test.salesforce.com"
const hostProduction = "https://login.salesforce.com"
const tokenPath = "/services/oauth2/token"

type testHost bool

func (h testHost) Host() string {
	if h {
		return hostTest
	}
	return hostProduction
}

// Config contains sufficient info for JWT Login
type Config struct {
	Host          string `json:"host,omitempty"`           /// salesforce host for instance
	ConsumerKey   string `json:"consumer_key,omitempty"`   // sf consumer key for application
	IsTest        bool   `json:"is_test,omitempty"`        // set to yes if using sandox
	UserID        string `json:"user_id,omitempty"`        // salesforce login for user impersonation
	Key           string `json:"key,omitempty"`            // private key pem
	KeyID         string `json:"keyid,omitempty"`          // optional
	APIVersion    string `json:"version,omitempty"`        // vXX.X, leave blank for salesforce default
	TokenDuration int    `json:"tokenDuration,omitempty"`  // in minutes
	CacheFile     string `json:"file_cache_loc,omitempty"` // path of file for use with a filecache.

	ClientFunc ctxclient.Func `json:"-"` // used for testing
}

// ServiceFromFile uses the passed file to create a Service
func ServiceFromFile(ctx context.Context, fn string, tc auth.TokenCache) (*salesforce.Service, error) {
	f, err := os.Open(fn)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	return ServiceFromReader(ctx, f, tc)
}

// ServiceFromReader uses the passed file to create a Service
func ServiceFromReader(ctx context.Context, rdr io.Reader, tc auth.TokenCache) (*salesforce.Service, error) {
	b, err := ioutil.ReadAll(rdr)
	if err != nil {
		return nil, err
	}
	return ServiceFromJSON(ctx, b, tc)
}

// ServiceFromJSON uses the passed byte array to create a Service
func ServiceFromJSON(ctx context.Context, buff []byte, tc auth.TokenCache) (*salesforce.Service, error) {
	var cx *Config
	if err := json.Unmarshal(buff, &cx); err != nil {
		return nil, err
	}
	if tc == nil && cx.CacheFile > "" {
		tc = &auth.FileCache{Filename: cx.CacheFile}
	}
	return cx.Service(ctx, tc)
}

func (c *Config) jwtcfg() (*jwt.Config, error) {
	if err := c.validate(); err != nil {
		return nil, err
	}
	key, err := jws.RS256FromPEM([]byte(c.Key), "")
	if err != nil {
		return nil, fmt.Errorf("invalid key: %v", err)
	}
	return &jwt.Config{
		Signer:         key,
		Issuer:         c.ConsumerKey,
		Audience:       testHost(c.IsTest).Host(),
		Subject:        c.UserID,
		TokenURL:       testHost(c.IsTest).Host() + tokenPath,
		HTTPClientFunc: c.ClientFunc,
	}, nil

}

var (
	errHostEmpty        = errors.New("host may not be empty")
	errConsumerKeyEmpty = errors.New("consumer_key may not be empty")
	errUserIDEmpty      = errors.New("user_id may not be empty")
)

func (c *Config) validate() error {
	if c.Host == "" {
		return errHostEmpty
	}
	if c.ConsumerKey == "" {
		return errConsumerKeyEmpty
	}
	if c.UserID == "" {
		return errUserIDEmpty
	}
	return nil
}

// Service returns api service authorizing api calls via jwt token gen
func (c *Config) Service(ctx context.Context, tc auth.TokenCache) (*salesforce.Service, error) {
	jwtcfg, err := c.jwtcfg()
	if err != nil {
		return nil, err
	}
	if tc == nil && c.CacheFile == "" {
		tc = &auth.FileCache{Filename: c.CacheFile}
	}
	var ops []auth.ClientFuncParam
	if c.TokenDuration > 0 {
		ops = append(ops, auth.TokenCacheDuration(time.Duration(c.TokenDuration)*time.Minute))
	}
	ccf, err := auth.CachingClientFunc(tc, jwtcfg, ops...)
	if err != nil {
		return nil, err
	}
	return salesforce.New(c.Host, c.APIVersion, nil).WithCtxClientFunc(ccf), nil
}

// FileCache uses filesystem to cache tokens in a predetermined file
type FileCache = auth.FileCache
