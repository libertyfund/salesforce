// Copyright 2022 James Cote
// All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package auth

import (
	"time"

	"github.com/jfcote87/oauth2"
)

func GetCredentials(ts oauth2.TokenSource, tc TokenCache, dur time.Duration) *Credentials {
	return &Credentials{
		ts:            ts,
		tc:            tc,
		tokenDuration: dur,
	}
}
